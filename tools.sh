#! /bin/bash

#Autor: Grzegorz Grobelny / yans

clear

choice=y;


while [ "$choice" = "y" ]; do

echo "---------------------------------------------------"
echo "--------     t o o l s       t o o l s     --------"
echo "---------------------------------------------------"
echo 
echo
echo "1.  ping"
echo "2.  traceroute"
echo "3.  netstat"
echo "4.  ip address"
echo "5.  random password maker"
echo "6.  uname"
echo "7.  file system"
echo "8.  memory info"
echo "9.  vmstat"
echo "10. time zone select"
echo "11. Empty trash"
echo "12. Neofetch"
echo "13. Users & w"
echo "14. User activity in the system"
echo "x.  close program"
echo
echo -n "choose number.: "

read a

case "$a" in

"1") echo -n "IP/domane name to PING : "
 read address
     echo -n "number's of packet's to send: "
	   read packet
	   ping -c $packet $address  ;;

"2") echo "IP to traceroute : "
	read address1
     traceroute $address1 
echo "traceroute are finished.." ;;

"3") netstat ;;
"4") ip address ;;

"5") echo -n 
     echo " Generate password with 20 random letters/number"
     echo " -s to add 1 special character"
     echo " -v to verbose mode"
     echo ""
    read
#read option
#    case ${option} in


# echo "This script generates a random password."
# echo "This user can set the password length with -l and a special character with -s."
# echo "Verbose mode can be enabled with -v."

usage() {
  echo "Usage: ${0} [-vs] [-l LENGTH]" >&2
  echo 'Generate a random password.'
  echo ' -l LENGTH  Specify the password length.'
  echo ' -s         Append a special character to the password.'
  echo ' -v         Increase verbosity.'
  exit 1
}


log() {
  local MESSAGE="${@}"
  if [[ "${VERBOSE}" = 'true' ]]
  then
    echo ""
    echo " ...and your password is:"
    echo ""
  fi
}


# Set a default password length
LENGTH='20'
VERBOSE='true'
USE_SPECIAL_CHARACTER='true'

while getopts vl:s OPTION
do
   case ${OPTION} in
     v)
       VERBOSE='true'
       log 'Verbose mode on.'
       ;;
     l)
       LENGTH="${OPTARG}"
       ;;
     s)
       USE_SPECIAL_CHARACTER='true'
       ;;
     ?)
       usage
       ;;
   esac
done

log 'Generating a password.'

PASSWORD=$(date +%s%N%${RANDOM}${RANDOM} | sha256sum | head -c${LENGTH})
 
# Append a special character if requested to do so.
if [[ "${USE_SPECIAL_CHARACTER}" = 'true' ]]
then
  log 'Selecting a random special character.'
  SPECIAL_CHARACTER=$(echo '!@#$%^&*()-_+=' | fold -w1 | shuf | head -c1)
  PASSWORD="${PASSWORD}${SPECIAL_CHARACTER}"
fi

#log 'Done.'
#log 'Here is the password:'

# Display the password.

echo "${PASSWORD}" ;;

"6") uname -a ;;
"7") df -h ;;
"8") cat /proc/meminfo ;;
"9") vmstat -s ;;
"10") sudo dpkg-reconfigure tzdata ;;
"11") sudo rm -rf ~/.local/share/Trash/*;;
"12") neofetch;;
"13") users echo && w;;
"14") last;;
"x") echo "program are finished."
     exit 1 ;;

*) echo "incorrect option" 

esac

echo
echo -n "Do you want to perform any other task? [ y/n ] : "
read choice
clear
done

exit 0


